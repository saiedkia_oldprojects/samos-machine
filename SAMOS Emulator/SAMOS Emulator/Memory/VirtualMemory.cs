﻿namespace SAMOS_Emulator.Memory
{
    public class VirtualMemory
    {
        public int ACC = 0;

        const int MEMORY_SIZE = 1000;
        object[] Memory = new object[MEMORY_SIZE];

        public object Read(int offset)
        {
            return Memory[offset];
        }

        public void Write(int offset, object  value)
        {
            Memory[offset] = value;
        }

        public void Clear()
        {
            Memory = new object[MEMORY_SIZE];
        }

    }
}
