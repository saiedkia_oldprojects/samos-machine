﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SAMOS_Emulator
{
    public partial class InputBox : Form
    {
        public InputBox()
        {
            InitializeComponent();
        }
        public void Texts(string Caption,string Text)
        {
            this.Text = Caption;
            lblMessage.Text = Text;
        }
        public string Read()
        {
            return txtValue.Text;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Read();
            this.Close();
        }

        private void InputBox_Load(object sender, EventArgs e)
        {
            txtValue.Focus();
        }
    }
}
