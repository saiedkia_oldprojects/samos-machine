﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SAMOS_Emulator.SAMOS
{
    public class CodeFromatter
    {
        StringBuilder errors = null;

        private string FormatLine(string line, int row)
        {
            string tmp = line.Replace(" ", "");
            if (tmp.Length != 10 && tmp.Length != 0)
            {
                errors.Append("Line " + row.ToString() + " : length is invalid!" + Environment.NewLine);
                return line;
            }
            else if (tmp == "") return tmp;

            return tmp.Substring(0, 3) + " " + tmp.Substring(3, 3) + " " + tmp.Substring(6, 4);
        }


        private string[] Format(string[] Code)
        {
            for (int i = 0; i <= Code.Count() - 1; i++)
                Code[i] = FormatLine(Code[i], i);

            return Code;
        }


        public Tuple<string, string[]> Prettify(string[] lines)
        {
            errors = new StringBuilder();

            string[] formatedCode = Format(lines);
            return (errors?.Length == 0) ? new Tuple<string, string[]>(null, lines) : new Tuple<string, string[]>(errors.ToString(), formatedCode);
        }
    }

    public static class StringArrayExtension
    {
        public static string AsString<T>(this IEnumerable<T> self)
        {
            StringBuilder stringBuilder = new StringBuilder();
            int count = self.Count() - 1;
            for (int i = 0; i <= count; i++)
            {
                if (i < count)
                    stringBuilder.Append(self.ElementAt(i).ToString() + Environment.NewLine);
                else
                    stringBuilder.Append(self.ElementAt(i).ToString());
            }

            return stringBuilder.ToString();
        }

        public static void AppendLine(this string str, string value)
        {
            str += value + Environment.NewLine;
        }
    }
}