﻿using System.Linq;
namespace SAMOS_Emulator.SAMOS
{
    class Compiler
    {

        public Memory.VirtualMemory Memory = new Memory.VirtualMemory();

        public string Compile(string[] Code)
        {

            bool exit = false;
            string res = null;
            string[] brekcode;
            int i = 0;
            #region Intialize 000
            try
            {
                for (i = 0; i <= Code.Count() - 1; i++)
                {

                    if (Code[i].Split(' ')[0] != "")
                    {
                        brekcode = Code[i].ToString().Split(' ');
                        int MemAdd = int.Parse(brekcode[2]);
                        int val = int.Parse(brekcode[1]);
                        if (Code[i].Split(' ')[0] == "000")
                        {
                            Memory.Write(i, MemAdd);
                        }
                    }
                }
            }
            catch
            {
                res += "initializeation Error!";
            }

            #endregion

            #region Compiler
            try
            {
                for (i = 0; i <= Code.Count() - 1; i++)
                {

                    if (Code[i].Split(' ')[0] != "")
                    {
                        brekcode = Code[i].ToString().Split(' ');
                        int MemoryAddress = int.Parse(brekcode[2]);
                        int value = int.Parse(brekcode[1]);
                        switch (Code[i].Split(' ')[0].ToUpper())
                        {

                            case "LDA":
                                {

                                    Memory.ACC = int.Parse(Memory.Read(MemoryAddress).ToString());
                                    break;
                                }
                            case "STO":
                                {
                                    Memory.Write(MemoryAddress, Memory.ACC);
                                    break;
                                }
                            case "ADD":
                                {
                                    Memory.ACC += int.Parse(Memory.Read(MemoryAddress).ToString());
                                    break;
                                }
                            case "SUB":
                                {
                                    Memory.ACC -= int.Parse(Memory.Read(MemoryAddress).ToString());
                                    break;
                                }
                            case "MUL":
                                {
                                    Memory.ACC *= int.Parse(Memory.Read(MemoryAddress).ToString());
                                    break;
                                }
                            case "DIV":
                                {
                                    Memory.ACC /= int.Parse(Memory.Read(MemoryAddress).ToString());
                                    break;
                                }
                            case "RWD":
                                {
                                    InputBox inp = new InputBox();
                                    inp.Texts("Read Value", "Enter Value:");
                                    inp.ShowDialog();
                                    Memory.Write(MemoryAddress, inp.Read());
                                    break;
                                }
                            case "WWD":
                                {
                                    res += Memory.Read(MemoryAddress).ToString() + System.Environment.NewLine;
                                    break;
                                }
                            case "BRU":
                                {

                                    i = MemoryAddress - 1;
                                    break;
                                }
                            case "BMI":
                                {
                                    if (Memory.ACC <= 0)
                                    {
                                        i = MemoryAddress - 1;
                                    }
                                    break;
                                }
                            case "HLT":
                                {
                                    exit = true;
                                    break;
                                }
                            case "000":
                                {
                                    res += "Line" + i.ToString() + " : 000 command couden't read/execute. use bmi/bru"
                                        + System.Environment.NewLine;
                                    exit = true;
                                    break;
                                }

                        }

                        if (exit == true)
                        {
                            break;
                        }

                    }
                }
            }
            catch
            {
                res += "---Ciritical Error!! Please check code/Values.---";
            }
            #endregion
            Memory.Write(999, i);
            return res;
        }
        public string[] MemoryValues()
        {
            string[] ReadMemory = new string[1000];
            ReadMemory[0] = "ACC=" + Memory.ACC.ToString();
            if (Memory.Read(999) != null)
            {
                ReadMemory[999] = Memory.Read(999).ToString();
            }
            int i = 1;
            for (i = 1; i <= 998; i++)
            {
                if (Memory.Read(i) != null)
                {
                    ReadMemory[i] = i.ToString() + "/" + Memory.Read(i).ToString();
                }
            }

            return ReadMemory;
        }
    }
}