﻿using System.Linq;

namespace SAMOS_Emulator.SAMOS
{
    public class Debugger
    {
        public string Debug(string[] Code)
        {

            string result = "";

            try
            {
                for (int i = 0; i <= Code.Count() - 1; i++)
                {
                    if (Code[i] == "") continue;

                    string[] brekcode = Code[i].ToString().Split(' ');
                    string command = brekcode[0].ToUpper();
                    int MemoryAddress = int.Parse(brekcode[2]);
                    int value = int.Parse(brekcode[1]);

                    if (value.ToString().Length <= 3 && value.ToString().Length >= 4)
                        result.AppendLine("Line" + i.ToString() + ": Value is not valuable!");

                    switch (command)
                    {
                        case "LDA":
                        case "STO":
                        case "ADD":
                        case "SUB":
                        case "MUL":
                        case "DIV":
                        case "RWD":
                        case "WWD":
                        case "000":
                            {
                                if (MemoryAddress > 999 || MemoryAddress < 1)
                                    result.AppendLine("Line" + i.ToString() + ": "
                                        + Code[i].Split(' ')[0].ToString().ToUpper()
                                        + " - memory address <999 and > 0");

                                if (value != 0)
                                    result.AppendLine("Line" + i.ToString() + ": "
                                        + Code[i].Split(' ')[0].ToString().ToUpper()
                                        + " LDA - value is not Variable");
                                break;
                            }
                        case "BRU":
                            {
                                if (MemoryAddress > Code.Count())
                                    result.AppendLine("Line" + i.ToString() + " : BRU - line number  " + MemoryAddress.ToString() +
                                        " not aviable!");
                                break;
                            }
                        case "BMI":
                            {
                                if (MemoryAddress > Code.Count())
                                    result.AppendLine("Line" + i.ToString() + " : BMI - line number  " + MemoryAddress.ToString() +
                                        " not aviable!");
                                break;
                            }
                        case "HLT":
                            {
                                if (MemoryAddress != 0000 || value != 0)
                                    result.AppendLine("Line" + i.ToString() + " : HLT command error bad command/value!");

                                break;
                            }
                        default:
                            {
                                result.AppendLine("Line" + i.ToString() + " : Unknow command!");
                                break;
                            }
                    }


                }//end for
            }//end of try
            catch
            {
                result += "--- Ciritical Error!! Please check code. ---";
            }

            return result;
        }
    }

}
