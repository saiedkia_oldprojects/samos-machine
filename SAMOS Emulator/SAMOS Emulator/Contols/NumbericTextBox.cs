﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SAMOS_Emulator.Contols
{
    public partial class NumbericTextBox : UserControl
    {

        public NumbericTextBox()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Return NumericTextbox text
        /// </summary>
        /// <returns></returns>
        public string Text()
        {
            string TextValue = string.Empty;
            //string LinesText = richcode.Text;
            int lines = richcode.Lines.Count() - 1;
            for (int i = 0; i <= lines; i++)
            {
                TextValue += richcode.Lines[i] + System.Environment.NewLine;

            }
            return TextValue;
        }
        private string tx;
        /// <summary>
        /// NumericTextBox text property
        /// </summary>
        public string TextValue
        {
            get
            {
                return tx;
            }

            set
            {
                tx = value;
                richcode.Text = tx;
            }
        }
        /// <summary>
        ///Retunrn text of line(line number)
        /// </summary>
        /// <param name="Line">int line number</param>
        /// <returns></returns>
        public string ReadLine(int Line)
        {

            string TextValue = string.Empty;
            //Line -= 1;
            TextValue = richcode.Lines[Line].ToString();
            return TextValue;
        }
        /// <summary>
        /// Count of Lines
        /// </summary>
        /// <returns>Count of Lines</returns>
        public int LinesCount()
        {
            int count = richcode.Lines.Count() - 1;
            return count;
        }
        private void richcode_TextChanged(object sender, EventArgs e)
        {

            int lines = richcode.Lines.Count();
            richLinenumber.Text = "";
            for (int i = 0; i <= lines - 1; i++)
            {
                richLinenumber.Text += i.ToString() + System.Environment.NewLine;
            }
        }
        /// <summary>
        /// Charecters count
        /// </summary>
        /// <returns>Charecters count</returns>
        public int Count()
        {
            int co = richcode.Text.Count();
            return co;
        }
        /// <summary>
        /// Send one backSpace to numericTextBox
        /// </summary>
        public void SendBackSpace()
        {
            string s = richcode.Text;
            richcode.Text = s.Substring(0, richcode.Text.Count()-1);
        }

    }
}