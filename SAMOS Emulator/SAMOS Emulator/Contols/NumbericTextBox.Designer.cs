﻿namespace SAMOS_Emulator.Contols
{
    partial class NumbericTextBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richcode = new System.Windows.Forms.RichTextBox();
            this.richLinenumber = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // richcode
            // 
            this.richcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.richcode.Location = new System.Drawing.Point(25, 0);
            this.richcode.Name = "richcode";
            this.richcode.Size = new System.Drawing.Size(377, 466);
            this.richcode.TabIndex = 2;
            this.richcode.Text = "";
            this.richcode.TextChanged += new System.EventHandler(this.richcode_TextChanged);
            // 
            // richLinenumber
            // 
            this.richLinenumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.richLinenumber.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.richLinenumber.Location = new System.Drawing.Point(3, 0);
            this.richLinenumber.Name = "richLinenumber";
            this.richLinenumber.Size = new System.Drawing.Size(45, 466);
            this.richLinenumber.TabIndex = 3;
            this.richLinenumber.Text = "";
            // 
            // NumbericTextBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.richcode);
            this.Controls.Add(this.richLinenumber);
            this.Name = "NumbericTextBox";
            this.Size = new System.Drawing.Size(409, 473);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richcode;
        private System.Windows.Forms.RichTextBox richLinenumber;
    }
}
