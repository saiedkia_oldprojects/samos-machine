﻿using SAMOS_Emulator.SAMOS;
using System;
using System.Windows.Forms;

namespace SAMOS_Emulator
{
    public partial class Editor : Form
    {
        Debugger debugger = new Debugger();
        CodeFromatter formatter = new CodeFromatter();
        Compiler compiler = new Compiler();
        SaveFileDialog savedialog = new SaveFileDialog();

        public Editor()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (numbericTextBox1.Text() != "")
            {
                compiler.Memory.Clear();
                txtOutput.Text = "";


                Tuple<string, string[]> result = formatter.Prettify(Read());
                if (result.Item1 != null)
                    txtOutput.Text = result.Item1;
                else
                {
                    numbericTextBox1.TextValue = result.Item2.AsString();
                    string debugResult = debugger.Debug(result.Item2);
                    if (string.IsNullOrEmpty(debugResult))
                        txtOutput.Text = compiler.Compile(result.Item2);
                    else
                        txtOutput.Text = debugResult;
                }
            }
        }



        private void button2_Click(object sender, EventArgs e)
        {
            //if (debugger.Debug(Read()) != null)
            //{
            //    State st = new State();
            //    st.Log(compiler.MemoryValues());
            //    st.ShowDialog();
            //}



        }

        #region Menu ...
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {

            AboutMe s = new AboutMe();
            s.ShowDialog();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (savedialog.FileName != "")
                {
                    System.IO.File.WriteAllText(savedialog.FileName, numbericTextBox1.Text());
                }
            }
            catch
            {
                MessageBox.Show("File not found \r\nUse Save As to save!", "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void newnewWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to save codes?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                savedialog.Filter = "Text File|*.txt";
                savedialog.ShowDialog();
                System.IO.File.WriteAllLines(savedialog.FileName, Read());
            }
            else
            {
                numbericTextBox1.TextValue = "";
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            savedialog.Filter = "Text File|*.txt";
            if (savedialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.File.WriteAllText(savedialog.FileName, numbericTextBox1.Text());
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog Open = new OpenFileDialog();
            Open.Filter = "Text File|*.txt";
            Open.ShowDialog();
            if (!string.IsNullOrEmpty(Open.FileName))
                numbericTextBox1.TextValue = System.IO.File.ReadAllText(Open.FileName);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure want to exit?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                this.Close();
            }

        }

        private void eventsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (numbericTextBox1.TextValue != "")
            {
                Clipboard.SetText(numbericTextBox1.Text());
            }
        }

        private void vPastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            numbericTextBox1.TextValue += Clipboard.GetText();
        }

        private void sAMOSCommandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string msg = "000 Read an value(int) -- 000 100 0001"
                    + "\r\nLDA Load memory(int) to ACC -- LDA 000 0002"
                    + "\r\nSTO save ACC value to memory(int) -- STO 000 0003"
                    + "\r\nADD ACC=ACC+memory(int) -- ADD 000 0001"
                    + "\r\nSUB ACC=ACC-memory(int) -- SUB 000 0005"
                    + "\r\nMUL ACC=ACC*memory(int) -- MUL 000 0003"
                    + "\r\nDIV ACC=ACC/memory(int) -- DIV 000 0008"
                    + "\r\n"
                    + "\r\nRWD ReadWord -- RWD 000 0001"
                    + "\r\nWWD write(memory(int)) -- WWD 000 0003"
                    + "\r\nBRU go to line (int) -- BRU 000 0012"
                    + "\r\nBMI go to line (int) if acc are odd -- BRU 000 0012"
                    + "\r\nHLT end -- HLT 000 0000";
            MessageBox.Show(msg, "SAMOS Commands");
        }

        private void stateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            State st = new State();
            st.Log(compiler.MemoryValues());
            st.ShowDialog();
        }
        #endregion


        private string[] Read()
        {
            int i = 0;
            string[] tx = new string[numbericTextBox1.LinesCount() + 1];
            for (i = 0; i <= numbericTextBox1.LinesCount(); i++)
            {
                tx[i] = numbericTextBox1.ReadLine(i);
            }
            return tx;
        }

        //private void ArraytoString(string[] Sorted)
        //{
        //    numbericTextBox1.TextValue = "";
        //    for (int i = 0; i <= Sorted.Count() - 1; i++)
        //    {
        //        numbericTextBox1.TextValue += Sorted[i] + Environment.NewLine;
        //    }
        //    if (debugger.Debug(Sorted) == "")
        //    {
        //        txtOutput.Text = compiler.Compile(Sorted);
        //        numbericTextBox1.Select();
        //        numbericTextBox1.SendBackSpace();
        //    }
        //    else
        //    {
        //        txtOutput.Text = debugger.Debug(Sorted);
        //    }
        //}
    }
}
