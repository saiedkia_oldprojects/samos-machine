﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SAMOS_Emulator
{
    public partial class State : Form
    {
        public State()
        {
            InitializeComponent();
        }

        private void State_Load(object sender, EventArgs e)
        {

        }
        public void Log(string[] Value)
        {
            ListViewItem list = new ListViewItem();
            string[] log = new string[999];


            lstLog.Items.Add("ACC").SubItems.Add(Value[0].ToString().Substring(4));
            lstLog.Items.Add("PC").SubItems.Add(Value[999]);
            for (int i = 1; i <= 998; i++)
            {
                if (Value[i] != null)
                {
                    lstLog.Items.Add(Value[i].ToString().Split('/')[0]).SubItems.Add(Value[i].ToString().Split('/')[1]);
                }
            }
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            lstLog.Items.Clear();
            this.Close();
        }
    }
}
